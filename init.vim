"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('~/.cache/dein')
  call dein#begin('~/.cache/dein')

  " Let dein manage dein
  " Required:
  call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')

  " Add or remove your plugins here like this:
  "call dein#add('Shougo/neosnippet.vim')
  "call dein#add('Shougo/neosnippet-snippets')
  call dein#add('easymotion/vim-easymotion')
  call dein#add('joshdick/onedark.vim')
  call dein#add('junegunn/fzf')
  call dein#add('overcache/NeoSolarized')
  call dein#add('scrooloose/nerdtree')
  call dein#add('Shougo/deoplete.nvim')
  call dein#add('tpope/vim-surround')

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

"End dein Scripts-------------------------

cd ~
colorscheme onedark
lang de_DE.UTF-8
let mapleader = ","
nmap Q <Nop> " 'Q' in normal mode enters Ex mode. You almost never want this.
nnoremap <C-P> :FZF<CR>
set backspace=indent,eol,start
set bg=dark
set cursorline
set guifont=Fira\ Code:h12
set hidden
set ignorecase
set incsearch
set laststatus=2
set mouse+=a
set noerrorbells visualbell t_vb=
set number
set relativenumber
set shortmess+=I
set smartcase